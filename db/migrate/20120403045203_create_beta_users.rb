class CreateBetaUsers < ActiveRecord::Migration
  def change
    create_table :beta_users do |t|
        t.string :FirstName
        t.string :LastName
        t.boolean :HasLoggedIn

      t.timestamps
    end
  end
end
